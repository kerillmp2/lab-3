#include <stdio.h>

unsigned long test_number;

int is_prime( unsigned long x ) {
	unsigned long i;

	/*1 is not prime*/
	if(x == 1) return 0;

	/*Check all possible dividers from 2 to sqrt(x)*/
	for( i = 2; i * i <= x; i++ ){
		if( x % i == 0 ) return 0;
	}

	/*If there are no dividers - given number is prime*/
	return 1;
}

int main( int argc, char** argv ) {
	puts("Enter unsigned number:");
	scanf("%lu", &test_number);
	printf( "The number %lu is%s prime\n", test_number, is_prime( test_number ) ? "" : " not" );
	return 0;
}
