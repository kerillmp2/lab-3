#include <stdio.h>

const int test_array1[] = {1, 2, 3};
const int test_array2[] = {6, 6, 6};

int scalar_product( const int* first_array, const int* second_array, size_t first_array_size, size_t second_array_size ) {
	size_t i;
	int scalar = 0;

	/*Return 0 if arrays have different sizes*/
	if( first_array_size != second_array_size ) {
		return 0;
	}

	/*If size is OK, count scalar product*/
	for( i = 0; i < first_array_size; i++ ) {
		scalar = scalar + ( first_array[i] * second_array[i] );
	}
	return scalar;
}

int main( int argc, char** argv ) {
	const int scalar = scalar_product( test_array1, test_array2, sizeof(test_array1) / sizeof(int), sizeof(test_array2) / sizeof(int));
	printf("The scalar product of given arrays is: %d\n", scalar);
	return 0;
}
